package com.example.workmanagerapp.utils

sealed class Resources<T>(data: T? = null, message: String? = null){
    data class Success<T>(val data: T): Resources<T>(data)
    data class Error<T>(val message: String): Resources<T>(null, message)
}
