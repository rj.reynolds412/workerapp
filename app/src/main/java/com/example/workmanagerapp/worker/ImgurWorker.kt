package com.example.workmanagerapp.worker

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.workmanagerapp.data.remote.ImgurApiService
import com.example.workmanagerapp.domain.models.UploadResponse
import com.example.workmanagerapp.utils.Constants.KEY_IMAGE_URI
import com.example.workmanagerapp.utils.Resources
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileOutputStream

class ImgurWorker(context: Context, workerParameters: WorkerParameters) :
    CoroutineWorker(context, workerParameters), ImgurUploader {
    @SuppressLint("RestrictedApi")
    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        return@withContext try {
            val imageUriInput =
                inputData.getString(KEY_IMAGE_URI) ?: return@withContext Result.failure()
            val response = uploadFile(Uri.parse(imageUriInput))
            when (response) {
                is Resources.Error -> {
                    Result.failure()
                }
                is Resources.Success -> {
                    val link = response.data.data.link
                    val outputData = workDataOf(KEY_IMAGE_URI to link)
                    Result.Success(outputData)

                }
            }
        } catch (e: Exception) {
            Result.failure()
        }
    }

    override val imgurApi: ImgurApiService = ImgurApiService.getInstance()

    override suspend fun uploadFile(uri: Uri, title: String?): Resources<UploadResponse> {
       return try {
            val file = copyStreamToFile(uri)
           val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
           val filePart = MultipartBody.Part.createFormData("image", file.name, requestFile)

           val response = imgurApi.uploadImage(image = filePart)
           if (response.isSuccessful){
               Resources.Success(response.body()!!)
           }else{
               Resources.Error("Something went Wrong")
           }
        } catch (e: Exception) {
            Resources.Error(e.localizedMessage ?: "Something went wrong")
        }
    }



    override suspend fun copyStreamToFile(uri: Uri): File {
        val outputFile = withContext(Dispatchers.IO) {
            File.createTempFile("temp", null)
        }
        applicationContext.contentResolver
            .openInputStream(uri)?.use { input ->
                val outputStream = FileOutputStream(outputFile)
                outputStream.use { output ->
                    val buffer = ByteArray(4 * 1024)
                    while (true) {
                        val byteCount = input.read(buffer)
                        if (byteCount < 0) break
                        output.write(buffer, 0, byteCount)
                    }
                    output.flush()
                }
            }
        return outputFile
    }
}

