package com.example.workmanagerapp.worker

import android.net.Uri
import com.example.workmanagerapp.data.remote.ImgurApiService
import com.example.workmanagerapp.domain.models.UploadResponse
import com.example.workmanagerapp.utils.Resources
import java.io.File

interface ImgurUploader {
    val imgurApi : ImgurApiService
    suspend fun uploadFile(uri: Uri, title: String? = null): Resources<UploadResponse>
    suspend fun copyStreamToFile(uri: Uri): File
}