package com.example.workmanagerapp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.example.workmanagerapp.ui.theme.WorkManagerAppTheme
import com.example.workmanagerapp.utils.Constants
import com.example.workmanagerapp.worker.ImgurWorker


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WorkManagerAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ShowScreen()
                }
            }
        }
    }
}

@Composable
fun ShowScreen() {
    val context = LocalContext.current
    var selectImage by remember { mutableStateOf<Uri?>(null) }
    val launcher = rememberLauncherForActivityResult(contract =
    ActivityResultContracts.GetContent()) { uri: Uri? ->
        selectImage = uri
    }
    Scaffold {
        it.calculateBottomPadding()
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Bottom
        ) {
            DisplayImage(selectImage, context)
            Row() {
                Button(onClick = { launcher.launch("image/*") },
                    shape = MaterialTheme.shapes.large,
                    modifier = Modifier.padding(12.dp)) {
                    Text(text = "Select a Photo")
                }
                Spacer(modifier = Modifier.width(10.dp))
                selectImage?.let {
                    Button(onClick = {
                        val inputData =
                            workDataOf(Constants.KEY_IMAGE_URI to selectImage.toString())
                        val uploadWorkRequest = OneTimeWorkRequestBuilder<ImgurWorker>()
                            .setInputData(inputData)
                            .build()
                        WorkManager.getInstance(context).enqueue(uploadWorkRequest)
                    },
                        shape = MaterialTheme.shapes.large,
                        modifier = Modifier.padding(12.dp)) {
                        Text(text = "Upload a Photo")
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayImage(
    selectImage: Uri?,
    context: Context,
) {
    val bitmap = remember {
        mutableStateOf<Bitmap?>(null)
    }
    selectImage?.let {
        if (Build.VERSION.SDK_INT < 28) {
            bitmap.value = MediaStore.Images
                .Media.getBitmap(context.contentResolver, it)
        } else {
            val source = ImageDecoder
                .createSource(context.contentResolver, it)
            bitmap.value = ImageDecoder.decodeBitmap(source)
        }
        bitmap.value?.let { btm ->
            Image(bitmap = btm.asImageBitmap(),
                contentDescription = null,
                modifier = Modifier.size(400.dp))
        }
    }
}


@Preview
@Composable
fun ShowScreenPreview() {
    WorkManagerAppTheme {
        ShowScreen()
    }
}