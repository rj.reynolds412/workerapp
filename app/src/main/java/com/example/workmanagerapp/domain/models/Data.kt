package com.example.workmanagerapp.domain.models


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("account_id")
    val accountId: Any?,
    @SerializedName("account_url")
    val accountUrl: Any?,
    @SerializedName("ad_type")
    val adType: Any?,
    @SerializedName("ad_url")
    val adUrl: Any?,
    val animated: Boolean,
    val bandwidth: Int,
    val datetime: Int,
    val deleteHash: String,
    val description: Any?,
    val favorite: Boolean,
    @SerializedName("has_sound")
    val hasSound: Boolean,
    val height: Int,
    val hls: String,
    val id: String,
    @SerializedName("in_gallery")
    val inGallery: Boolean,
    @SerializedName("in_most_viral")
    val inMostViral: Boolean,
    @SerializedName("is_ad")
    val isAd: Boolean,
    val link: String,
    val mp4: String,
    val name: String,
    val nsfw: Any?,
    val section: Any?,
    val size: Int,
    val tags: List<Any>,
    val title: Any?,
    val type: String,
    val views: Int,
    val vote: Any?,
    val width: Int
)