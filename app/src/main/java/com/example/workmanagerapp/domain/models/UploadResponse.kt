package com.example.workmanagerapp.domain.models


import com.google.gson.annotations.SerializedName

data class UploadResponse(
    val data: Data,
    val status: Int,
    val success: Boolean
)